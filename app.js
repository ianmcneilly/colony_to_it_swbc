/*
* @Author: McneilI
* @Date:   2018-09-20 12:01:59
* @Last Modified by:   McneilI
* @Last Modified time: 2018-09-21 11:45:22
*/

//date format in 2nd field needs to match that of DOB. dd/mm/yyyy
/*


*/



require("dotenv").config();
var request = require("request-promise");
var cheerio = require("cheerio");
var stringify = require('csv-stringify');
var dateFormat = require('dateformat');
//$ = cheerio.load(req)
//
const fs = require('fs'); 
const csv = require('csv-parser');

var Connection = require("tedious").Connection;
var config ={
    userName: process.env.DBUSER,  
        password: process.env.DBPASS,  
        server: process.env.DBSERVER,  
        // If you are on Microsoft Azure, you need this:  
        //options: {encrypt: true, database: 'AdventureWorks'} 
}
var connection = new Connection(config); 
connection.on('connect', function(err) {  
    // If no error, then good to proceed.  
    console.log("Connected");  
    executeStatement();
}); 

var Request = require('tedious').Request;  
var TYPES = require('tedious').TYPES;  

function executeStatement() {  
    request = new Request(`
    SELECT 
    [EnquiryID]
    ,[EnquiryTypeID]
    ,[XML]
    ,[FormXML]
    ,[Created]
FROM [Iceland].[dbo].[core_Enquiry] 
WHERE EnquiryTypeID = 37 
and Created > '2018-09-16 00:00:00.000'
ORDER BY Created desc
    `, function(err) {  
    if (err) {  
        console.log(err);}  
    });  
    var result = "";  
    request.on('row', function(columns) {  
        try{
            var data = {
                FormXML : columns[3].value,
                Created : columns[4].value,
            }  
            //console.log(data);
            addrow(data);
            //console.log("hi");
        }
        catch(err){
            console.log("error!!! ===== >" + err)
        }
    });  

    request.on('requestCompleted', function(rowCount, more) {  
        console.log(rowCount + ' rows returned');  
        stringify(
            table,
            {
                delimiter:"|",
                rowDelimiter : "windows",
            },
            function(err,output){
                console.log(output);
                fs.appendFileSync(outfile,output);
            }
        );
        connection.close();
    });  
    connection.execSql(request);  
}  

//process.exit();

var table = [];
var count = 0;
var outputTableTitles = [
    "DataID",
    "DateCaptured",
    "URN1",
    "URN2",
    "Title",
    "FirstName",
    "LastName",
    "Address1",
    "Address2",
    "Address3",
    "Address4",
    "Town",
    "County",
    "Postcode",
    "PAFChecked",
    "DateOfBirth",
    "HomeTelephone",
    "MobileTelephone",
    "Email",
    "OptOutIceland",
    "OptOut3rdParty",
    "Date",
    "Answer1",
    "Answer2",
    "Answer3",
    "SlimmingWorld",
    "ConsentTsCs",
    "ConsentTsCsDateTime",
    "IcelandConsent",
    "IcelandConsentDateTime",
    "ThirdPartyConsent",
    "ThirdPartyConsentDateTime",
]
table.push(outputTableTitles);

var outfile = "output_" + new Date().toISOString().replace(/(:|\.|\s)/g,"_") + ".csv";

function addrow(data){
    $ = cheerio.load(data.FormXML);
    var urn = $("number_eb64f548-8144-4fb8-87de-60e6ff721894").text();
    if(urn.length !== 19){return;}

    function getUrns(){
        var urn = $("number_eb64f548-8144-4fb8-87de-60e6ff721894").text()
        var urn2; //other half or all depending
        urn1 = urn.substr(0,8);
        urn2 = urn.substr(8,11);
        
        return {
            urn1 : urn1,
            urn2: urn2,
        }
    }

    var TitleMap = {
        "FieldOption1" : "Miss",
        "FieldOption2" : "Mrs",
        "FieldOption3" : "Ms",
        "FieldOption4" : "Mr",
        "FieldOption5" : "Dr",
    }

    var yesNoMap = {
        "FieldOption1" : "Y",
        "FieldOption2" : "N",
    }

    var noYesMap = {
        "FieldOption1" : "0",
        "FieldOption2" : "1",
    }

    //filter blank addresses.
    var address1 = $("number_d63fac9e-a73f-44e4-95b0-49cb5ad76b43").text().replace(/FieldOption1|FieldOption2/g,"");
    var address2 = $("text_b5067733-1e0d-4b98-952e-0cf91614c7b0").text().replace(/FieldOption1|FieldOption2/g,"");
    var address3 = $("text_3e24731d-262e-4215-87d0-4dcd028f5c69").text().replace(/FieldOption1|FieldOption2/g,"");

   
    var outputTableMap = [
        "RG0006", //nope DataID
        /*"DateCaptured" :*/ dateFormat(new Date(data.Created),"dd/mm/yyyy"), //created date 
        /*"URN1" :*/ getUrns().urn1,
        /*"URN2" :*/ getUrns().urn2,
        /*"Title" :*/ TitleMap[$("dropbox_12d5bdca-0b50-482f-8312-ecdf1f91f0ca").text()],
        /*"FirstName" :*/ $("text_c3ac97bd-1230-434d-8d9a-fb7476b23fab").text(),
        /*"LastName" :*/ $("text_c3c544e2-1c50-4ebc-a5ea-1d74ec31cfa8").text(),
        /*"Address1" :*/ address1,
        /*"Address2" :*/ address2,
        /*"Address3" :*/ address3, //no
        /*"Address4" :*/ "", //no 
        /*"Town" :*/ $("text_74e17d3e-79bb-496a-88b6-33eaef70732c").text(),
        /*"County" :*/ $("text_a21b9598-7e73-4aa3-b7e6-84962ade248b").text(),
        /*"Postcode" :*/ $("text_5384c13d-6c78-4954-8c29-ce84d7aff602").text(),
        /*"PAFChecked" :*/ "0", //no
        /*"DateOfBirth" :*/ $("datetime_9d927b0f-7fa4-4b12-9034-0eaf5c952389").text().substr(0,10),
        /*"HomeTelephone" :*/ $("number_97b6a281-3494-4c1c-bd46-80a4ed55a932").text().replace(/FieldOption1|FieldOption2/g,""),
        /*"MobileTelephone" :*/ $("number_1a5c6f93-cfae-4e1a-9c92-4f610517ed1f").text().replace(/FieldOption1|FieldOption2/g,""),
        /*"Email" :*/ $("email_abe1831e-eb54-4767-a7da-9c3f10d801cc").text() ,
        /*"OptOutIceland" :*/ noYesMap[$("radio_59a6f2d8-1830-4572-9774-0123dc31c992").text()],
        /*"OptOut3rdParty" :*/ noYesMap[$("radio_3474fbf4-42fe-47b3-af50-d5d444ab508c").text()],
        /*"Date" :*/  dateFormat(new Date(data.Created),"dd/mm/yyyy"),
        /*"Answer1" :*/ "", //no
        /*"Answer2" :*/ "", //no need
        /*"Answer3" :*/ "", //no
        /*"SlimmingWorld" :*/ "Y",
        /*"ConsentTsCs" :*/ "Y", //always yes
        /*"ConsentTsCsDateTime":*/ dateFormat(new Date(data.Created),"dd/mm/yyyy") + " " + new Date(data.Created).toLocaleTimeString(), 
        /*"IcelandConsent" :*/ yesNoMap[$("radio_59a6f2d8-1830-4572-9774-0123dc31c992").text()],
        /*"IcelandConsentDateTime":*/ dateFormat(new Date(data.Created),"dd/mm/yyyy") + " " + new Date(data.Created).toLocaleTimeString(),
        /*"ThirdPartyConsent":*/ yesNoMap[$("radio_3474fbf4-42fe-47b3-af50-d5d444ab508c").text()],
        /*"ThirdPartyConsentDateTime" :*/dateFormat(new Date(data.Created),"dd/mm/yyyy") + " " + new Date(data.Created).toLocaleTimeString(),
    ]

    //filter blank addresses.
    var address1 = $("number_d63fac9e-a73f-44e4-95b0-49cb5ad76b43").text().replace(/FieldOption1|FieldOption2/g,"");
    var address2 = $("text_b5067733-1e0d-4b98-952e-0cf91614c7b0").text().replace(/FieldOption1|FieldOption2/g,"");
    var address3 = $("text_3e24731d-262e-4215-87d0-4dcd028f5c69").text().replace(/FieldOption1|FieldOption2/g,"");
    if(address1 === "" && address2 === "" && address3 === ""){return;}

    

    for(var i = 0; i < outputTableMap.length; i++){
        // if(outputTableMap[i].indexOf('"') > -1 ){return;}
        // if(outputTableMap[i].indexOf("'") > -1 ){return;}
        // if(outputTableMap[i].indexOf(",") > -1 ){return;}
        // if(outputTableMap[i].toUpperCase().indexOf("SELECT") > -1 ){return;}
        outputTableMap[i] = outputTableMap[i].replace(/\||\"|\'|SELECT|,/gi,"").substr(0,50);
    }

    table.push(outputTableMap);
    count++;
    console.log(count);
}


